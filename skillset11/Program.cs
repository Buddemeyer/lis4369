﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double num1;
            Console.Write("Please enter a number:");
           

            while(!double.TryParse(Console.ReadLine(),out num1))
            {
            	Console.Write("Please use an integer");
            }

            Console.WriteLine("Call PassByValue().");
            PassByValue(num1);
            Console.WriteLine("In Main() num1 = " + num1);
            PassByRef(ref num1);
            Console.WriteLine("In Main() num1 = " + num1);


       }
    	
    	static void PassByValue(double numm)
    	{
    		Console.WriteLine("Inside PassByValue() method");
    		numm = numm +5;
    		Console.WriteLine("Added 5 to number passed by value = " + numm);
    	
    	}

    	static void PassByRef(ref double num)
    	{
    		Console.WriteLine("Inside PassByRef() method");
    		num = num +5;
    		Console.WriteLine("Added 5 to number passed by ref = " + num);
    	
    	

    	}



    }
}
