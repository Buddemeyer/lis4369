> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Nicholas Buddemeyer

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of hwapp aplication running
* Screenshot of aspnetcoreapp application running [My NET Core Installation](http://localhost:5000)
* git commands w/short descriptions;

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates empty Git repository 
2. git status - displays paths that have differences between the index file and current HEAD commit
3. git add - add file contents to the index
4. git commit - record changes to repository
5. git push - update remote refs along with associated objects
6. git pull - fetch from and integrate with another repo or local branch
7. git clone - creates a working copy of a local repository 

#### Assignment Screenshots:

*Screenshot of aspnetcoreapp application running [My NET Core Installation](http://localhost:5000)*:

![aspnetcoreapp application running](img/aspnetcoreapp_a1.png)

*Screenshot of hwapp aplication running*:

![hwapp aplication running](img/hwapp_a1.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Buddemeyer/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Buddemeyer/myteamquotes/ "My Team Quotes Tutorial")
