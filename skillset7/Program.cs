﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication
{
     public class Program:Employee
{ 
  static void Main(string[] args)
        {	
        	DateTime today = DateTime.Now;
		
        Console.WriteLine("Now: {0:dddd MM/dd/yy H:mm:ss }\n", today);


        	Employee e = new Employee();
            

            Console.WriteLine("\nModify defualt construtor object's data member values:\nUse setter/getter methods:");
            Console.Write("First Name:");
            e.setFname(Console.ReadLine());
            Console.Write("Last Name:");
            e.setLname(Console.ReadLine());

            Console.WriteLine("\nDisplay object's new data member values:");
            Console.WriteLine("First Name:" + e.getFname() + "\nLast Name:" + e.getLname());

            Console.WriteLine("\nCall parameterized construtor (accepts two arguments):");
             Console.Write("First Name:");
            string a = Console.ReadLine();
            Console.Write("Last Name:");
            string b = Console.ReadLine();

            Employee c = new Employee(a,b);
            Console.WriteLine("\nCreating person object from parameterized construtor (accepts two arguments");
            c.print();


            Console.WriteLine("\n\nPress any key to exit.");
   			 Console.ReadKey();




            
            
           

           
        }

  }         
}
