> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Nicholas Buddemeyer

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "A1 README")
    * Install .NET Core
    * Create hwapp application
    * Create aspnetcoreapp application
	* Provide Screenshots of install
	* Create Bitbucket Repo




2. [A2 README.md](a2/README.md "A2 README")
	* Simple Calculator Program 
	* Images of Valid and Invalid Operations





3. [A3 README.md](a3/README.md "A3 README")
	* Room Calculator 
	* Image of Room Calculator


4. [P1 README.md](proj1/README.md "P1 README")
	* Room Calculator

5. [A4 README.md](a4/README.md "A4 README")
	* Inheritance Person 
	* images of Program
	
6. [A5 README.md](a5/README.md "A5 README")
	* Car/Vehical 
	* images of Program 

7. [P2 README.md](proj2/README.md "P2 README")
	* Intro to LINQ



	

