﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {	DateTime today = DateTime.Now;
		
        Console.WriteLine("Now: {0:dddd MM/dd/yy H:mm:ss }\n", today);

        

        Console.WriteLine("for loop:");
       
        for (int i = 1; i <= 10; i++)
        {	

        	Console.Write(i + " ");
        }

        Console.WriteLine("\n\nwhile loop:");
        int num = 1;
        
        while(num <= 10)
        {
        	Console.Write(num + " ");
        	num ++;
        }

        Console.WriteLine("\n\n do while loop:");
        num = 1;

        do
        {
        	Console.Write(num + " ");
        	num ++;


        }while(num <= 10);
		Console.WriteLine("\n\nfor each loop:");
        
        string[] array = new string [] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
       

       foreach (string element in array)
       {
       	Console.Write(element + " ");
       }

       Console.WriteLine("\n\nPress any key to exit.");
   			 Console.ReadKey();



        }
    }
}
