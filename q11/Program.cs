﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            Vehicle vehicle1 = new Vehicle();
            Console.WriteLine(vehicle1.GetObjectInfo());

            string p_manufacturer ="";
            string p_make ="";
            string p_model ="";
            string p_seperator ="";
            float p_miles = 0.0f;
            float p_gallons = 0.0f;

            Console.Write("Manufacturer (alpha):");
            p_manufacturer = Console.ReadLine();
            Console.Write("Make:");
            p_make = Console.ReadLine();
            Console.Write("Model:");
            p_model = Console.ReadLine();





            Console.Write("Miles driven:");
            while(!float.TryParse(Console.ReadLine(),out p_miles))
            {
            	Console.WriteLine("Must be numeric");
            }
            
             Console.Write("Gallons used:");
            while(!float.TryParse(Console.ReadLine(),out p_gallons))
            {
            	Console.WriteLine("Must be numeric");
            }
        	
        	
            Console.WriteLine("\nVehicle2 - instantiating new object passing only first arg:");

            Vehicle v2 = new Vehicle(p_manufacturer);

             Console.WriteLine(v2.GetObjectInfo());

            

            Console.Write("Delimiter (, : ;)");

            p_seperator = Console.ReadLine();

            Console.WriteLine(v2.GetObjectInfo(p_seperator));

            Console.WriteLine("\nvehicle3 -(instantiating new object passing *all* vehicle args):");

            Vehicle v3 = new Vehicle(p_manufacturer,p_make,p_model);

            v3.SetGallons(p_gallons);
            v3.SetMiles(p_miles);

            Console.Write("Delimiter (, : ;)");
            p_seperator = Console.ReadLine();
            Console.WriteLine(v3.GetObjectInfo(p_seperator));



            Console.WriteLine("\nDemonstrating Polymorphism (new derived object:");
            	string p_style = "";
               
                Console.Write("Manufacturer (alpha):");
           		p_manufacturer = Console.ReadLine();
            	Console.Write("Make:");
            	p_make = Console.ReadLine();
            	Console.Write("Model:");
            	p_model = Console.ReadLine();
            	

  			

            	Console.Write("Miles driven:");
            while(!float.TryParse(Console.ReadLine(),out p_miles))
            {
            	Console.WriteLine("Must be numeric");
            }
            
             Console.Write("Gallons used:");
            while(!float.TryParse(Console.ReadLine(),out p_gallons))
            {
            	Console.WriteLine("Must be numeric");
            }

            Console.Write("Style:");
                p_style = Console.ReadLine();

            Console.WriteLine("\ncar1 -(instantiating new derived object, passing *all* args):");
            Car c1 = new Car(p_manufacturer,p_make,p_model,p_style);
    
            c1.SetGallons(p_gallons);
            c1.SetMiles(p_miles);
			Console.Write("Delimiter (, : ;)");

             p_seperator = Console.ReadLine();

            Console.WriteLine(c1.GetObjectInfo(p_seperator));



            Console.WriteLine();
            Console.WriteLine("\nPress any key to exit");
            Console.ReadKey();
			


        }




    }
}
