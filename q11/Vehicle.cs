using System;

namespace ConsoleApplication
{
public class Vehicle
{
private float milesTraveled, gallonsUsed;
	
	public string Manufacturer{get;set;}
	public string Make{get;set;}
	public string Model {get;set;}

	public float MPG
	{
		get{
			if(gallonsUsed <= 0.0f)
			{
				return 1.0f;
			}
			else 
			{
				return milesTraveled / gallonsUsed;
			}
		}
	}


public Vehicle()
{
	this.Manufacturer = "Mazda";
	this.Make = "Mazda";
	this.Model = "RX8";
	Console.WriteLine("\nCreating base object from defualt constructor (accepts no argument");



}
public Vehicle(string mn = "Manufacturer", string mk ="Make", string md ="Model")
{
	this.Manufacturer = mn;
	this.Make= mk;
	this.Model= md;
	Console.WriteLine("\nCreating base object from parameterized constructor (accepts argument");
}

public void SetMiles(float m = 0.0f)
{
	milesTraveled = m;
}





public void SetGallons(float g = 0.0f)
{
	gallonsUsed = g;
}


public float GetMiles()
{
	return milesTraveled;

}

public float GetGallons()
{
	return gallonsUsed;
}

public virtual string GetObjectInfo()
{
	return Manufacturer + "," + Make + "," + Model + "," + MPG;
}

public virtual string GetObjectInfo(string sep)
{
	return Manufacturer + sep + Make + sep + Model + sep + MPG;
}
}
}