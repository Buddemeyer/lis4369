using System;

namespace ConsoleApplication
{
public class Car:Vehicle
{
private string style;
	
	

	public string Style{
		get{return style;}
		set{style = value;}
	}


public Car()
{

this.Style="Default Style";
	Console.WriteLine("\nCreating base object from defualt constructor (accepts no argument");
}
public Car(string mn = "Manufacturer", string mk ="Make", string md ="Model", string st ="Style")
:base(mn,mk,md)
{
	
		this.Style = st;
	
	Console.WriteLine("\nCreating base object from parameterized constructor (accepts argument");
}





public override string GetObjectInfo(string sep)
{
	return base.GetObjectInfo(sep) + sep + Style;
}
}
}