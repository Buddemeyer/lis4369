> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Nicholas Buddemeyer

### Assignment 3 Requirements:

*Sub-Heading:*

1. Future Calculator



#### README.md file should include the following items:

* Screenshot of Simple Calculator Valid Operation
* Screenshot of Simple Calculator Invalid Operation


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Simple Calculator Valid Operation *:

![Simple Calculator Valid Operation](img/img.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Buddemeyer/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Buddemeyer/myteamquotes/ "My Team Quotes Tutorial")
