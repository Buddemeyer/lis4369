﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Value Returning Functions \nAuthor: Nicholas Buddemeyer");
       		DateTime today = DateTime.Now;
		
        Console.WriteLine("Now: {0:dddd MM/dd/yy H:mm:ss }\n", today);

        double num1;
        double num2;
        double ans;
        int op;
        Console.Write("num1: ");
       num1 = Convert.ToDouble(Console.ReadLine());

       Console.Write("num2: ");
       num2 = Convert.ToDouble(Console.ReadLine());
        string[] array = new string [] { "Addition", "Subtraction", "Multiplication", "Division","Power"};

              

       Console.WriteLine("1 - Addition\n2 - Subtraction\n3 - Multiplication\n4 - Division\n5 - Exponentiation");
       Console.Write("\nChoose a mathematical operation: ");
        
        op = Convert.ToInt32(Console.ReadLine());

        
        if (op == 1)
       ans = Addition(num1,num2);
        else if (op == 2)
       ans = Subtraction(num1,num2);
        else if( op == 3)
       ans = Multiplication(num1,num2);
        else if (op ==4 && num2 ==0 ) 
         {Console.WriteLine("Invlaid operation, cannot divide by 0");
         return;}
      else if(op==4 && num2 !=0) 
       ans = Division(num1,num2);
        else if(op == 5)
        ans = Power(num1,num2);
        else 
        {Console.WriteLine("An incorrect mathematical operation was entered");
        return;	
        }	


        
         Console.WriteLine("\n*** Result of " + array[op - 1] + " operation:  ***\n" + ans ); 
  


       	

       Console.WriteLine("\n\nPress any key to exit.");
   			 Console.ReadKey();

        }
    
        public static double  Addition(double numm, double numm1)
        { double ans;
          ans = numm + numm1 ;
          
          return ans;  
        }

         public static double Subtraction(double numm, double numm1)
        {double ans;
          ans = numm - numm1 ;
           
          return ans;
        }


        public static double Multiplication(double numm, double numm1)
        {double ans;
          ans = numm * numm1 ;
           
          return ans;
        }


        public static double Division(double numm, double numm1)
        {double ans;
           
         ans = numm / numm1;
          
          
          return ans;  
        }
        


        public static double Power(double numm, double numm1)
        {double ans;
          ans = Math.Pow(numm,numm1);
          
          return ans;  
        }


   



    }
}