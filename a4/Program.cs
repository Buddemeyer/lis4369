﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime today = DateTime.Now;
		
        Console.WriteLine("Now: {0:dddd MM/dd/yy H:mm:ss }\n", today);


        	Person p1 = new Person();
            
            Console.Write("First Name:");
            Console.WriteLine(p1.getFname());
            Console.Write("Last Name:");
            Console.WriteLine(p1.getLname());
            Console.Write("Age:");
            Console.WriteLine(p1.getAge());


            Console.WriteLine("\nModify person object's data member values:\nUse setter/getter methods:");

            
            Console.Write("First Name:");
            string p_fname = Console.ReadLine();
            Console.Write("Last Name:");
            string p_lname = Console.ReadLine();

            int p_age = 0;

            Console.Write("Age:");
            while(!int.TryParse(Console.ReadLine(),out p_age))
            {
            	Console.WriteLine("Please use an integer.");
            }
            

			p1.setFname(p_fname);
			p1.setLname(p_lname);
			p1.setAge(p_age);

			Console.WriteLine("\nDisplay object's new data member values:");
			Console.Write("First Name:");
			Console.WriteLine(p1.getFname());
			Console.Write("Last Name:");
            Console.WriteLine(p1.getLname());
            Console.Write("Age:");
            Console.WriteLine(p1.getAge());

            Console.WriteLine("\nCall parameratized base constructor (accepts arguments):");
            Console.Write("First Name:");
            p_fname = Console.ReadLine();
            Console.Write("Last Name:");
            p_lname = Console.ReadLine();
            Console.Write("Age:");
            while(!int.TryParse(Console.ReadLine(),out p_age))
            {
            	Console.WriteLine("Please use an integer.");
            }

            Person p2 = new Person(p_fname,p_lname,p_age);
            Console.Write("First Name:");
            Console.WriteLine(p2.getFname());
            Console.Write("Last Name:");
            Console.WriteLine(p2.getLname());
            Console.Write("Age:");
            Console.WriteLine(p2.getAge());

            Console.WriteLine("\nCall derived defualt constructor ( inherts from base class):");
            Student s1 = new Student();

            Console.Write("First Name:");
            Console.WriteLine(s1.getFname());
            Console.Write("Last Name:");
            Console.WriteLine(s1.getLname());
            Console.Write("Age:");
            Console.WriteLine(s1.getAge());

            Console.WriteLine("\nDemonstrating Polymorphism (new derived object):");

            
            Console.Write("First Name:");
            string s_fname = Console.ReadLine();
            Console.Write("Last Name:");
            string s_lname = Console.ReadLine();

            int s_age = 0;

            Console.Write("Age:");
            while(!int.TryParse(Console.ReadLine(),out s_age))
            {
            	Console.WriteLine("Please use an integer.");
            }

            Console.Write("College:");
            string s_college = Console.ReadLine();
            Console.Write("Major:");
            string s_major = Console.ReadLine();

            double s_gpa = 0;
            Console.Write("gpa:");
            while(!double.TryParse(Console.ReadLine(),out s_gpa))
            {
            	Console.WriteLine("Please use an integer.");
            }

            Student s2 = new Student(s_fname,s_lname,s_age,s_college,s_major,s_gpa);

            Console.Write("person2 - GetObjectInfo (virtual): \n");
            	Console.WriteLine(p2.GetObjectInfo());

            	 Console.Write("\nstudent2 - GetObejctInfo (overridden): \n");
            	Console.WriteLine(s2.GetObjectInfo());







            Console.WriteLine("\n\nPress any key to exit.");
   			 Console.ReadKey();



        }
    }
}
