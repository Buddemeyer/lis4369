using System;
namespace ConsoleApplication
{
public class Person
    {
        protected string fname;
   		protected string lname;
      protected int age;
    
   

public Person()
{
   
   fname = "John";
   lname ="Doe";
   age = 0;
   Console.WriteLine("\nCreating person object from defualt construtor (accepts no arguments)");
           
}

public Person(string namef="", string namel="", int agea=0)
{
   
   fname = namef;
   lname = namel;
   age = agea;
   Console.WriteLine("\nCreating person object from parametized construtor (accepts arguments)");
   }

public string getFname()
{
   return fname;
}

public void setFname(string fn ="")
{
   fname = fn;
}

public string getLname()
{
   return lname;
}

public void setLname(string ln ="")
{
   lname = ln;
}

public int getAge()
{
   return age;
}

public void setAge(int ag =0)
{
   age = ag;
}



public virtual string GetObjectInfo()
{
  return fname + " " + lname + " " + age.ToString();
}
}
}