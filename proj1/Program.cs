﻿using System;

namespace ConsoleApplication
{
    public class Program:Room
    {
        public static void Main(string[] args)
        {
           DateTime today = DateTime.Now;
		
        Console.WriteLine("Now: {0:dddd MM/dd/yy H:mm:ss }\n", today);
        double ln = 0.0;
            double height= 0.0;
           double width= 0.0;
            int count = 0;
           string b = " ";

        	Room r = new Room();
           

            Console.WriteLine("\nModify defualt construtor object's data member values:\nUse setter/getter methods:");
            Console.Write("Room Type:");
            r.setType(Console.ReadLine());
            
            while(count < 5)
      		{

      			if( count == 0)
      			{
      				Console.Write("Room Length:");
             b = Console.ReadLine();
      					
      					if( double.TryParse(b,out ln))
      						{
      							
      							count = 1;
      						r.setLength(ln);
      						}
      						else
      						 Console.WriteLine("Length must be numeric");
				}

				else if( count == 1)
      			{
      				Console.Write("Room Width: ");
      					b = Console.ReadLine();

      					
      					if( double.TryParse(b,out width))
      						{
      							
      							count = 2;
      							r.setWidth(width);
      						}
      						else
      						 Console.WriteLine("Width must be numeric");
				}

				else if( count == 2)
      			{
      				Console.Write("Room Height: ");
      					b = Console.ReadLine();
      					
      					if( double.TryParse(b,out height))
      						{
      							
      							count =6;
      						    r.setHeight(height);
      						}
      						else
      						 Console.WriteLine("Height must be numeric");
				}

				}
            

            Console.WriteLine("\nDisplay object's new data member values:");
            Console.WriteLine("Room:" + r.getType() + "\nLength:" + r.getLength() + "\nWidth:" + r.getWidth() + "\nHeight:" + r.getHeight() + "\nArea:" + r.getArea().ToString("F2")+ " sq ft" + "\nVolume:" + r.getVolume().ToString("F2")+ " cu ft" + "\nVolume:" + r.getVolumeyd().ToString("F2")+ " cu yd") ;

            Console.WriteLine("\nCall parameterized construtor (accepts two arguments):");
            
             Console.Write("Room Type:");
             string e = Console.ReadLine();
           	 count = 0;
            
            while(count < 5)
      		{

      			if( count == 0)
      			{
      				Console.Write("Room Length:");
             b = Console.ReadLine();
      					
      					if( double.TryParse(b,out ln))
      						{
      							
      							count = 1;
      						}
      						else
      						 Console.WriteLine("Length must be numeric");
				}

				else if( count == 1)
      			{
      				Console.Write("Room Width: ");
      					b = Console.ReadLine();

      					
      					if( double.TryParse(b,out width))
      						{
      							
      							count = 2;
      						}
      						else
      						 Console.WriteLine("Width must be numeric");
				}

				else if( count == 2)
      			{
      				Console.Write("Room Height: ");
      					b = Console.ReadLine();
      					
      					if( double.TryParse(b,out height))
      						{
      							
      							count =6;
      						}
      						else
      						 Console.WriteLine("Height must be numeric");
				}

				}

            
            

            Room a = new Room(e,ln,height,width);
            Console.WriteLine("\nCreating person object from parameterized construtor (accepts two arguments");
            	Console.WriteLine("Room:" + a.getType() + "\nLength:" + a.getLength() + "\nWidth:" + a.getWidth() + "\nHeight:" + a.getHeight() + "\nArea:" + a.getArea().ToString("F2")+ " sq ft" + "\nVolume:" + a.getVolume().ToString("F2")+ " cu ft" + "\nVolume:" + a.getVolumeyd().ToString("F2")+ " cu yd" );
            


            Console.WriteLine("\n\nPress any key to exit.");
   			 Console.ReadKey();



        }
    }
}
