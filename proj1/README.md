> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Nicholas Buddemeyer

### Project 1 Requirements:

*Sub-Heading:*

1. Room Calculator



#### README.md file should include the following items:

* Screenshot of Room Calculator 



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Room Calculator *:

![Room Calculator](img/img.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Buddemeyer/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Buddemeyer/myteamquotes/ "My Team Quotes Tutorial")
