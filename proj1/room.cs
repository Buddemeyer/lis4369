using System;
namespace ConsoleApplication
{
public class Room
    {
      private string type;
   		private double length;
      private double width;
      private double height;
      private double area;
      private double volume;
      private double volumeyd;
    
   

public Room()
{
   
   type = "Defualt";
   length = 10.0;
   width = 10.0;
   height = 10.0;
   area = length * width;
   volume = length * width * height;
   volumeyd = volume * .037037;
    Console.WriteLine("\nCreating room object from defualt construtor (accepts no arguments)");
            Console.WriteLine("Room:" + this.type + "\nLength:" + this.length + "\nWidth:" + this.width + "\nHeight:" + this.height + "\nArea:" + this.area.ToString("F2")+ " sq ft" + "\nVolume:" + this.volume.ToString("F2")+ " cu ft" + "\nVolume:" + this.volumeyd.ToString("F2")+ " cu yd" ) ;
}

public Room(string type1, double length1, double width1, double height1)
{
   
   type = type1;
   length = length1;
   width = width1;
   height = height1;
   }

public string getType()
{
   return type;
}

public void setType(string ty)
{
   type = ty;
}

public double getLength()
{
   return length;
}

public void setLength(double ln)
{
   length = ln;
}
public double getWidth()
{
   return width;
}

public void setWidth(double wd)
{
   width = wd;
}
public double getHeight()
{
   return height;
}

public void setHeight(double ht)
{
   height = ht;
}
public double getArea()
{ area = length * width;
   return area;
}
public double getVolume()
{ volume = length * width* height;
   return volume;
}
public double getVolumeyd()
{ volumeyd = volume * .037037;
   return volumeyd;
}




}
}