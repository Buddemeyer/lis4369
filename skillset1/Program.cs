﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
           String name;
            Console.Write("Please enter your full name:");
        
            name = Console.ReadLine();

            Console.WriteLine("Hello " + name + "!");
       
            Console.WriteLine("Press any key to exit.");
   			 Console.ReadKey();
        }
    }
}
