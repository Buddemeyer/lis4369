﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Non-Value Returning (void) Functions \nAuthor: Nicholas Buddemeyer");
       		DateTime today = DateTime.Now;
		
        Console.WriteLine("Now: {0:dddd MM/dd/yy H:mm:ss }\n", today);

        double num1;
        double num2;
        
        int op;
        Console.Write("num1: ");
       num1 = Convert.ToDouble(Console.ReadLine());

       Console.Write("num2: ");
       num2 = Convert.ToDouble(Console.ReadLine());
              

       Console.WriteLine("1 - Addition\n2 - Subtraction\n3 - Multiplication\n4 - Division\n5 - Exponentiation");
       Console.Write("\nChoose a mathematical operation: ");
        
        op = Convert.ToInt32(Console.ReadLine());

        
        if (op == 1)
        Addition(num1,num2);
        else if (op == 2)
        Subtraction(num1,num2)2;
        else if( op == 3)
        Multiplication(num1,num2);
        else if (op ==4 ) 
        Division(num1,num2);
        else if(op == 5)
         Power(num1,num2);
        else 
        {Console.WriteLine("An incorrect mathematical operation was entered");
        return;	
        }	
        
        
  


       	

       Console.WriteLine("\n\nPress any key to exit.");
   			 Console.ReadKey();

        }
    
        public static void Addition(double numm, double numm1)
        { double ans;
          ans = numm + numm1 ;
          Console.WriteLine("\n*** Result of Addition operation:  ***\n" + ans );  
        }

         public static void Subtraction(double numm, double numm1)
        {double ans;
          ans = numm - numm1 ;
          Console.WriteLine("\n*** Result of Subtraction operation:  ***\n" + ans );  
        }


        public static void Multiplication(double numm, double numm1)
        {double ans;
          ans = numm * numm1 ;
          Console.WriteLine("\n*** Result of Multiplication operation:  ***\n" + ans );  
        }


        public static void Division(double numm, double numm1)
        {double ans;
           if( numm1 == 0)
          {
            Console.WriteLine("Invlaid operation, cannot divide by 0");
            
          }
        else 
         { ans = numm / numm1;
          
          Console.WriteLine("\n*** Result of Division operation:  ***\n" + ans );  
        }
        }


        public static void Power(double numm, double numm1)
        {double ans;
          ans = Math.Pow(numm,numm1);
          Console.WriteLine("\n*** Result of Power operation:  ***\n" + ans );  
        }


   



    }
}