﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("A2: Simple Calculator \nNote: Does not perfrom data validation \nAuthor: Nicholas Buddemeyer");
       		DateTime today = DateTime.Now;
		
        Console.WriteLine("Now: {0:dddd MM/dd/yy H:mm:ss }\n", today);

        double num1;
        double num2;
        double ans = 0.0;
        int op = 0;
        int check = 1;
       string[] array = new string [] { "Addition", "Subtraction", "Multiplication", "Division"};
       
        while(check == 1)
       { Console.Write("\nnum1: ");
       num1 = Convert.ToDouble(Console.ReadLine());

       Console.Write("num2: ");
       num2 = Convert.ToDouble(Console.ReadLine());
               

       Console.WriteLine("1 - Addition\n2 - Subtraction\n3 - Multiplication\n4 - Division");
       Console.Write("\nChoose a mathematical operation: ");
        
        op = Convert.ToInt32(Console.ReadLine());

        
        if (op == 1)
        ans = num1 + num2;
        else if (op == 2)
        ans = num1 - num2;
        else if( op == 3)
        ans = num1 * num2;
       else if( op == 4 && num2 == 0)
        	{
        		Console.WriteLine("Invlaid operation, cannot divide by 0");
        		
        	}
        else if (op ==4 && num2 != 0) 
        	ans = num1 / num2;
        else 
        {Console.WriteLine("An incorrect mathematical operation was entered");
        return;	
        }	
       
        if (op == 4 && num2 ==0)
        check = 1;
        else
        check =2;
             
      };
        


        
        
  


       Console.WriteLine("\n*** Result of " + array[op - 1] + " operation:  ***\n" + ans );	

       Console.WriteLine("\n\nPress any key to exit.");
   			 Console.ReadKey();

        }
    }
}
