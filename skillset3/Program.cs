﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {	DateTime today = DateTime.Now;
		
        Console.WriteLine("Now: {0:dddd MM/dd/yy H:mm:ss }\n", today);

        	decimal bill = 100.00m;
        	int book;
        	int choice;
        	string color = " ";
        	Console.WriteLine("Your bill is $" + bill);
        	Console.WriteLine("Discounts:\n 6+: 30% off \n 3-5: 20% off \n 2: 10% off \n 1: Regular price");
        	Console.Write("How many books did you buy?");

        		book = Convert.ToInt32(Console.ReadLine());

        		if( book>= 6)
       			bill = bill * .7m;
       			else if (book >= 3 && book <= 5 )
       			bill = bill * .8m;
       			else if (book == 2 )
       			bill = bill * .2m;
       			else if (book == 1 )
       			bill = bill;
       			else
       			bill = 1;

       			if (bill == 1)
       			Console.WriteLine("Invalid entry");
       			else 
       			Console.WriteLine("Your cost: $" + bill);

       			Console.WriteLine("\n***************Switch Example**************************\n");

       			Console.WriteLine(" 1 - red \n 2 - green \n 3 - blue\n");
       			Console.Write("What is your favorite color?");
       			choice = Convert.ToInt32(Console.ReadLine());

       			switch(choice)
       			{
					case 1:
					color = "red";
					break;
					case 2:
					color ="green";
					break;
					case 3:
					color ="blue";
					break;
					
				}
       
       Console.WriteLine("Your favorite color is " + color);


            Console.WriteLine("Press any key to exit.");
   			 Console.ReadKey();

        }
    }
}
