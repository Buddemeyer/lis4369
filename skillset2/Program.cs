using System;
using System.Globalization;
namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double tempf;
            double tempc;
            double eq;
            
            Console.Write("Fahrenheit temperature:");
        
            tempf = Convert.ToDouble(Console.ReadLine());
            eq = (tempf - 32) * 5 / 9 ;


            Console.WriteLine(tempf +"F"+ " = " + eq +"C");

             Console.Write("Celsius temperature:");
        
            tempc = Convert.ToDouble(Console.ReadLine());
            eq = (tempc * 9) / 5 + 32;


            Console.WriteLine(tempc + "C"+ " = " + eq +"F");

       
            Console.WriteLine("Press any key to exit.");
   			 Console.ReadKey();
        }
    }
}
